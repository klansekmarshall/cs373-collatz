#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List
import math

cycle_cache = {
    1: 0,
    2: 1,
    4: 2,
    8: 3,
    16: 4,
    32: 5,
    64: 6,
    128: 7,
    256: 8,
    512: 9,
    1024: 10,
    2048: 11,
    4096: 12,
    8192: 13,
    16384: 14,
    32768: 15,
    65536: 16,
    131072: 17,
    262144: 18,
    524288: 19,
}
range_cache = {
    (1, 10000): 262,
    (10001, 20000): 279,
    (20001, 30000): 308,
    (30001, 40000): 324,
    (40001, 50000): 314,
    (50001, 60000): 340,
    (60001, 70000): 335,
    (70001, 80000): 351,
    (80001, 90000): 333,
    (90001, 100000): 333,
    (100001, 110000): 354,
    (110001, 120000): 349,
    (120001, 130000): 344,
    (130001, 140000): 344,
    (140001, 150000): 375,
    (150001, 160000): 383,
    (160001, 170000): 370,
    (170001, 180000): 347,
    (180001, 190000): 365,
    (190001, 200000): 360,
    (200001, 210000): 373,
    (210001, 220000): 386,
    (220001, 230000): 368,
    (230001, 240000): 443,
    (240001, 250000): 368,
    (250001, 260000): 363,
    (260001, 270000): 407,
    (270001, 280000): 407,
    (280001, 290000): 389,
    (290001, 300000): 371,
    (300001, 310000): 371,
    (310001, 320000): 384,
    (320001, 330000): 384,
    (330001, 340000): 366,
    (340001, 350000): 441,
    (350001, 360000): 379,
    (360001, 370000): 410,
    (370001, 380000): 423,
    (380001, 390000): 436,
    (390001, 400000): 405,
    (400001, 410000): 405,
    (410001, 420000): 449,
    (420001, 430000): 418,
    (430001, 440000): 400,
    (440001, 450000): 369,
    (450001, 460000): 387,
    (460001, 470000): 444,
    (470001, 480000): 382,
    (480001, 490000): 413,
    (490001, 500000): 426,
    (500001, 510000): 426,
    (510001, 520000): 470,
    (520001, 530000): 408,
    (530001, 540000): 377,
    (540001, 550000): 452,
    (550001, 560000): 421,
    (560001, 570000): 421,
    (570001, 580000): 390,
    (580001, 590000): 434,
    (590001, 600000): 403,
    (600001, 610000): 403,
    (610001, 620000): 447,
    (620001, 630000): 509,
    (630001, 640000): 416,
    (640001, 650000): 416,
    (650001, 660000): 429,
    (660001, 670000): 442,
    (670001, 680000): 385,
    (680001, 690000): 398,
    (690001, 700000): 442,
    (700001, 710000): 504,
    (710001, 720000): 411,
    (720001, 730000): 411,
    (730001, 740000): 424,
    (740001, 750000): 393,
    (750001, 760000): 424,
    (760001, 770000): 468,
    (770001, 780000): 437,
    (780001, 790000): 406,
    (790001, 800000): 468,
    (800001, 810000): 406,
    (810001, 820000): 450,
    (820001, 830000): 450,
    (830001, 840000): 525,
    (840001, 850000): 419,
    (850001, 860000): 419,
    (860001, 870000): 388,
    (870001, 880000): 432,
    (880001, 890000): 445,
    (890001, 900000): 370,
    (900001, 910000): 445,
    (910001, 920000): 476,
    (920001, 930000): 476,
    (930001, 940000): 507,
    (940001, 950000): 383,
    (950001, 960000): 414,
    (960001, 970000): 414,
    (970001, 980000): 458,
    (980001, 990000): 427,
    (990001, 1000000): 440,
}

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
	i the beginning of the range, inclusive
	j the end       of the range, inclusive
	return the max cycle length of the range [i, j]
	"""
    if i > j:
        i, j = j, i
    lens = []
    if j - i >= 10000:
        lower = (math.ceil(i / 10000) * 10000) + 1
        upper = math.floor(j / 10000) * 10000
        lens.append(get_max_cycle(i, lower))
        lens.append(get_max_cycle(upper, j))
        for n in range(lower, upper, 10000):
            lens.append(range_cache[(n, n + 9999)])
    else:
        lens.append(get_max_cycle(i, j))
    return max(lens)


# -------------
# get_max_cycle
# -------------


def get_max_cycle(i, j):
    max_cylce_len = 0
    for n in range(i, j + 1):
        cycle_len = get_cycle(n)
        if cycle_len > max_cylce_len:
            max_cylce_len = cycle_len
    return max_cylce_len


# ---------
# get_cycle
# ---------


def get_cycle(num_to_calc):
    """
	num_to_calc the number to calculate the cycle of
	cache the cache that we are using
	return the cycle length of num_to_calc
	"""
    global cycle_cache
    cycle = 1
    n = num_to_calc
    while n != 1:
        if n in cycle_cache.keys():
            cycle += cycle_cache[n]
            n = 1
        elif n % 2 == 0:
            cycle += 1
            n = n >> 1
        else:
            cycle += 2
            n += (n >> 1) + 1
    cycle_cache[num_to_calc] = cycle - 1
    if num_to_calc < 500000:
        cycle_cache[num_to_calc * 2] = cycle
    return cycle


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
